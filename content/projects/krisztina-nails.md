---
title: Krisztina Nails
type: page
---

### How it all started

As my wife started off a new small business, I decided to create a website for her.
At the beginning I was not well aware of the best web-tech, so I created the original site with React.

{{< figure src="/images/krisztina_nails/old_react_site.png" >}}

### How it continued

Since the react site was not fully set up so that the site user could use it, I decided to try out a new POV.

I wanted the following:

* site should be quick
* the user could create own contents
* a technology, that is also fun to use
* have a new easy styling option

So I came together with HUGO Static sites, and netlify CMS. The last part was a little tricky, I did not want to
create something pre-owned, such as hugo themed site, so I went on to use TailwindCSS and create my own theme.

### How it looks like now

Now the basic frame is deployed, and the older react site is replaced.
Some tweaking is always needed, and for the future also some extensions are planned.

The site can be accessed [here](https://www.krisztina-nails.at)

At the moment it has following features:

* hugo site themed with tailwindCSS
* basic webpage with gallery, contact info and price list
* a blog feature, where in the future, videos, or small topics can be uploaded
* the whole site can be extended by CMS





